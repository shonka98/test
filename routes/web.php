<?php

Self::get('/addad/{([0-9])/*([0-9]*)/*([0-9]*)}', 'AdController@add', 'addAd');
self::get('/category/{([0-9])/*([0-9]*)/*([0-9]*)((/page-[0-9]+)*)}', 'CategoryController@action', 'category');
Self::get('/admin/publication/{([0-9]+)}', 'AdminController@publication', 'adminPublication');
Self::get('/admin/reject/{([0-9]+)}', 'AdminController@publish', 'admin-reject');
Self::get('/updatead/{([0-9]+)}', 'AdController@update');
Self::get('/delete/{([0-9]+)}', 'AdController@delete');
Self::get('/userpage/{([0-9]+)}', 'UserController@userPage' , 'userpage');
Self::get('/userpage/add/{([0-9]+)}', 'UserController@addSub', 'userPageSubscribe');
Self::get('/ad/{([0-9]+)}', 'AdController@ad', 'ad');
Self::get('/register', 'UserController@register', 'register');
Self::get('/login', 'UserController@login', 'login');
Self::get('/resetPass', 'UserController@resetPass', 'resetPass');
Self::get('/logout', 'UserController@logout', 'logout');
Self::get('/categories', 'CategoryController@categories', 'categories');
Self::get('/mypage', 'UserController@mypage', 'mypage');
Self::get('/selectcategory', 'AdController@selectCategory', 'selectAd');
Self::get('/admin', 'AdminController@login', 'admin');
Self::get('/search', 'MainController@search');
Self::get('/{(page-[0-9]+)*}', 'MainController@action', 'index');

Self::get('/api/*(.+?)', 'Api@run', 'api');