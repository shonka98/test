@extends('masterUs')

@section('title', 'Главная')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Добавление объявления </div>
                <div class="card-header">Выбор категеорий</div>

                <div class="card-body">
                    <form method="POST" action="#" aria-label="Login">
                        @foreach($categories as $category)
                        <ul class="tree" id="tree">
                            <li> <a href="{{ \routes\Route::RouteName('addAd', $category['id']) }}" >{{$category['name']}}</a>
                                <ul>
                                    @foreach(\app\models\Category::getLowCategory($category['id']) as $low)
                                    <li> <a href="{{ \routes\Route::RouteName('addAd', $category['id'], $low['id']) }}" > {{$low['name']}}</a>
                                        <ul>
                                            @foreach(\app\models\Category::getLowerCategory($low['id']) as $lower)
                                            <li> <a href="{{ \routes\Route::RouteName('addAd' , $category['id'], $low['id'], $lower['id']) }}" >{{$lower['name']}}</a></li>

                                            @endforeach
                                        </ul>
                                    </li>
                                    @endforeach
                                </ul>
                            </li>
                        </ul>
                        @endforeach


                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection