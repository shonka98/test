@extends('master')

@section('title', 'Главная')

@section('content')

    <div class="starter-template">
        <form method="POST" action="#" aria-label="Login">
            <div class="form-group row">
                    <input id="email" type="text" class="form-control"
                           name="text" value="" required autofocus>
            </div>
            <div class="form-group row mb-0">

                    <button type="submit" name="submit" class="btn btn-primary">
                        Поиск
                    </button>
            </div>
        </form>
        <h1>Найденые товары</h1>
        <div class="row">
            <div class="row">
                @if(!$ads)
                    <h1>Ничего не найдено</h1>
                    @endif
                @foreach($ads as $ad)
                    @include('ad', compact('ad'))
                @endforeach
            </div>
        </div>
        <nav>

            <ul class="pagination">

                <li class="page-item disabled" aria-disabled="true" aria-label="pagination.previous">
                    <span class="page-link" aria-hidden="true">&lsaquo;</span>
                </li>


                <li class="page-item active" aria-current="page"><span class="page-link">1</span></li>
                <li class="page-item"><a class="page-link" href="?&amp;page=2">2</a></li>


                <li class="page-item">
                    <a class="page-link" href="?&amp;page=2" rel="next" aria-label="pagination.next">&rsaquo;</a>
                </li>
            </ul>
        </nav>

@endsection