<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Интернет магазин: @yield('title')</title>

    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <script src="/../../public/js/jquery.min.js"></script>
    <script src="/../../public/js/bootstrap.min.js"></script>
    <link href="/../../public/css/bootstrap.min.css" rel="stylesheet">
    <link href="/../../public/css/starter-template.css" rel="stylesheet">
    <link href="/../../public/css/my.css" rel="stylesheet">
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ \routes\Route::RouteName('index') }}">Интернет Магазин</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li {{ \components\Auxiliary::routeActive('') }}><a href="{{ \routes\Route::RouteName('index') }}">Последние объявления</a></li>
                <li {{ \components\Auxiliary::routeActive('categor*') }}><a href="{{ \routes\Route::RouteName('categories') }}">Категории</a>
                </li>

            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li><a href="{{ \routes\Route::RouteName('selectAd') }}">Добавить обьъявление</a></li>
            @if($_COOKIE['user'])
                    <li><a href="{{ \routes\Route::RouteName('mypage')}}">{{ \app\models\User::getUserById($_COOKIE['user'])->name }}</a></li>
                    <li><a href="{{ \routes\Route::RouteName('logout') }}">Выйти</a></li>
                @else

                    <li><a href="{{ \routes\Route::RouteName('login') }}">Войти</a></li>
                @endif
            </ul>
        </div>
    </div>
</nav>

<div class="container">

@yield('content')

</div>
</body>
</html>