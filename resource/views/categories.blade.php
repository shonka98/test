@extends('master')

@section('title', 'Категории')

@section('content')

    <div class="starter-template">

        <h1>Категории</h1>
        <table class="table">
            @foreach($categories as $category)
                <tr>
                    <th>
                        <h2> <a href="{{ \routes\Route::RouteName('category',$category['id'] )}}"> {{ $category['name'] }}</a> </h2>
                    </th>
                </tr>

                @foreach( \app\models\Category::getLowCategory($category['id']) as $low)
                    <tr>
                        <td>
                            <h4> <a href="{{  \routes\Route::RouteName('category',$category['id'] .'/'. $low['id'] )}}"> {{ $low['name'] }}</a> </h4>
                        </td>
                    </tr>

                    @foreach(\app\models\Category::getLowerCategory($low['id']) as $lower)
                        <tr>
                            <td>
                                <h6><a href="{{ \routes\Route::RouteName('category', $category['id'] .'/'. $low['id'] .'/'. $lower['id'] )}}">{{ $lower['name'] }}</a> </h6>
                            </td>
                        </tr>
                    @endforeach
                @endforeach
            @endforeach

        </table>
@endsection