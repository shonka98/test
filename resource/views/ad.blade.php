<div class="col-sm-6 col-md-4">
    <div class="thumbnail">
        <div class="labels">
        </div>
        <img src="http://internet-shop.tmweb.ru/storage/products/iphone_x.jpg" alt="iPhone X 64GB">
        <div class="caption">
            <h3 style="text-align: left"><a href="{{ \routes\Route::RouteName('ad', $ad->id) }}" >{{ $ad->name }}</a></h3>
            <p class="contentText"><b>{{ $ad->price }}₽</b></p>
            <p class="contentText"><span  class="pudo">{{ $ad->date }}</span></p>
        </div>
    </div>
</div>

