@extends('masterUs')

@section('title', 'Главная')

@section('content')


    <div class="row justify-content-center">
        <div class="col-md-12">
            <h1>Аминка </h1>
            <h1>Запросы на добавление </h1>

            <table class="table">
                <tbody>
                <tr>
                    <th>
                        #
                    </th>
                    <th>
                        Имя
                    </th>
                    <th>
                        Описание
                    </th>
                    <th>
                        Цена
                    </th>
                    <th>
                        Статус
                    </th>
                    <th>
                        Фотик
                    </th>
                    <th>
                        Подтвердить
                    </th>
                    <th>
                        Отколнить
                    </th>


                </tr>
                @foreach($ads as $ad)
                    <tr>
                        <td>
                            {{ $ad['id'] }}
                        </td>
                        <td>
                            {{ $ad['name'] }}
                        </td>
                        <td>
                            {{ $ad['description'] }}
                        </td>
                        <td>
                            {{ $ad['price'] }}
                        </td>
                        <td>
                            {{ $ad['status'] }}
                        </td>
                        <td>
                            {{ $ad['img'] }}
                        </td>
                        <td>
                           <a href="{{  \routes\Route::RouteName('adminPublication', $ad['id']) }} " >Опубликовать</a>
                        </td>
                        <td>
                            <a href="{{ \routes\Route::RouteName('admin-reject', $ad['id'][0]) }} " >Отколнить</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            </tbody>
            </table>

        </div>
    </div>


@endsection