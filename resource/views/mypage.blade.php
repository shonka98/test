@extends('masterUs')

@section('title', 'Главная')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-12">
            <h1>В расмотрении </h1>

            <table class="table">
                <tbody>
                <tr>
                    <th>
                        #
                    </th>
                    <th>
                        Имя
                    </th>
                    <th>
                        Описание
                    </th>
                    <th>
                        Цена
                    </th>
                    <th>
                        Ввыполнено
                    </th>


                </tr>
                @foreach($adsNoPublic as $ad)
                <tr>
                    <td>
                        {{ $ad->id }}
                    </td>
                    <td>
                        {{ $ad->name }}
                    </td>
                    <td>
                        {{ $ad->description }}
                    </td>
                    <td>
                        {{ $ad->price }}
                    </td>
                    <td>
                        {{ $ad->status }}
                    </td>
                    <tr>
                        <td>
                            @foreach(\components\Auxiliary::getLinks($ad->img) as $array)
                                <div class="mypageimg">
                                    <img src="{{ ROOT_PATH_IMG .'/'. $array }}"  width="200" height="222"  alt="Письма мастера дзен"></p>
                                </div>
                            @endforeach
                        </td>
                    </tr>

                </tr>
                    @endforeach
                </tbody>
            </table>
                </tbody>
            </table>

        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <h1>Опубликованные </h1>

            <table class="table">
                <tbody>
                <tr>
                    <th>
                        #
                    </th>
                    <th>
                        Имя
                    </th>
                    <th>
                        Описание
                    </th>
                    <th>
                        Цена
                    </th>
                    <th>
                        Ввыполнено
                    </th>


                </tr>
                @foreach($adsPublic as $ad)
                    <tr>
                        <td>
                            {{ $ad->id }}
                        </td>
                        <td>
                            {{ $ad->name }}
                        </td>
                        <td>
                            {{ $ad->description }}
                        </td>
                        <td>
                            {{ $ad->price }}
                        </td>
                        <td>
                            {{ $ad->status }}
                        </td>
                        <td>
                            {{ $ad->img }}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            </tbody>
            </table>

        </div>
    </div>


@endsection