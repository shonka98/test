@extends('masterUs')

@section('title', 'Главная')

@section('content')
<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">Авторизация</div>

            <div class="card-body">
                <form method="POST" action="#" aria-label="Login">
                    <div class="form-group row">
                        <label for="email" class="col-sm-4 col-form-label text-md-right">E-Mail</label>

                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control"
                                   name="email" value="" required autofocus>
                        </div>
                    </div>
                    <div class="form-group row mb-0">
                        <div class="col-md-8 offset-md-4">
                            <button type="submit" name="resetPass" class="btn btn-primary">
                                Забыл пароль
                            </button>
                            {{ $error }}
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection