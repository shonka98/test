<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Админка: Авторизация</title>

    <!-- Scripts -->
    <script src="/js/app.js" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="/../../public/css/app.css" rel="stylesheet">
    <link href="/../../public/css/bootstrap.min.css" rel="stylesheet">

</head>
<body>
<div id="app">
    <nav class="navbar navbar-default navbar-expand-md navbar-light navbar-laravel">
        <div class="container">
            <a class="navbar-brand" href="{{ \routes\Route::RouteName('index') }}">
                Вернуться на сайт
            </a>

            <div id="navbar" class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                </ul>

                <ul class="nav navbar-nav navbar-right">
                    <li class="nav-item">
                        @if($_SESSION['user'])
                        <a class="nav-link" href="{{ \routes\Route::RouteName('logout') }}">Выйти</a>
                        @else
                            <a class="nav-link" href="{{ \routes\Route::RouteName('login') }}">Войти</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ \routes\Route::RouteName('register') }}">Зарегистрироваться</a>
                    </li>
                        @endif

                </ul>

            </div>
        </div>
    </nav>

    <div class="py-4">
        <div class="container">
           @yield('content')
        </div>
    </div>
</div>
</body>
</html>
