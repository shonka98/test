@extends('masterUs')

@section('title', 'Главная')

@section('content')


    <div class="row justify-content-center">
        <div class="col-md-12">
            <h1>Пользователь</h1>

            <table class="table">
                <tbody>
                <tr>
                    <th>
                        #
                    </th>
                    <th>
                        Имя
                    </th>
                    <th>
                        Телефон
                    </th>
                    <th>
                        Оценка 1/5
                    </th>
                    <th>
                        Ввыполнено
                    </th>
                    <th>
                        Действия
                    </th>
                </tr>
                <tr>
                    <td>
                        {{ $user->id }}
                    </td>
                    <td>
                        {{ $user->name }}
                    </td>
                    <td>
                        {{ $user->phone }}
                    </td>
                    <td>
                        {{ $user->status }}
                    </td>
                    <td>
                        Ввыполнено
                    </td>
                    <td>
                        {{ $user->ing }}
                    </td>
                </tr>
                </tbody>
            </table>
            <h1>Объявления пользователя</h1>
            <table class="table">
                <tbody>
                <tr>
                    <th>
                        #
                    </th>
                    <th>
                        Имя
                    </th>
                    <th>
                        Фото
                    </th>
                    <th>
                        Количество просмотров
                    </th>
                    <th>
                        Описание
                    </th>
                    <th>
                        Цена
                    </th>
                </tr>
                @foreach($ads as $ad)
                <tr>
                    <td>
                        {{ $ad->id  }}
                    </td>
                    <td>
                        {{ $ad->name  }}
                    </td>
                    <td>
                        {{ $ad->img  }}
                    </td>
                    <td>
                        {{ $ad->count_view  }}
                    </td>
                    <td>
                        {{ $ad->description  }}
                    </td>
                    <td>
                        {{ $ad->price }}
                    </td>
                </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
        @if($_SESSION['user'] and \app\models  \Friend::checkFriendExists($_SESSION['user'], $user->id, 1))
        <h1><a href="{{ \routes\Route::RouteName('userPageSubscribe', $user->id) }}" >Подписаться</a> </h1>
            @endif
    </div>


@endsection