@extends('master')

@section('title', 'Объявление')

@section('content')
    <div class="starter-template left">

        <div class="sellers-info">
            <div class="sellers-info">

                <div class="seller-text-info">
                    <div>
                        <div>
                            <a href="{{ \routes\Route::RouteName('userpage', $seller->id) }}" class="">
                                {{ $seller->name }}
                            </a>
                        </div>
                        <div class="seller-rating">
                            <span><b>{{$seller->status}}/5 Оценка</b> </span>
                            <span>11 отзывов</span>
                        </div>
                    </div>
                    <div class="seller-class">Частное лицо</div>

                    <div>
                        <div class="seller-date">
                            На Авито c {{$seller->date}}
                        </div>
                    </div>
                </div>
                <div class="seller-avatar">
                    <a class="seller-avatar-image" title="Нажмите, чтобы перейти в профиль"
                       style="background-image: url('https://46.img.avito.st/avatar/social/256x256/6273631546.jpg')">Профиль</a>
                </div>

            </div>

                <div class="seller-ann-count">
                <a class="button-size" >121 объявление пользователя</a>
                </div>
                <span>
                    <button class="button-size" >
                        <span><a href="{{ \routes\Route::RouteName('userPageSubscribe', $seller->id) }}">Подписаться на пользователя</a>
                        </span>
                    </button>
                </span>

        </div>
        <h1>{{ $ad->name  }}</h1>
        <?php $a = \app\models\Category::getCategoryName($ad->category_id, $ad->low_category_id, $ad->lower_category_id);?>
        <h2> {{ $a[0][0].' / '.$a[0][1].' / '.$a[0][2]}}</h2>
        <p>Цена: <b>{{ $ad->price }} ₽</b></p>
        <img src="http://internet-shop.tmweb.ru/storage/products/iphone_x.jpg">
        <p>
           {{ $ad->description }}
        </p>



            <button type="submit" class="btn btn-success" role="button"> {{$seller->phone}} </button>


    </div>
@endsection
