@extends('master')

@section('title', 'Главная')

@section('content')

    <div class="starter-template">
        <h1>Все товары1</h1>
        <div class="row">
            <div class="row">
                @foreach($ads as $ad)
                    @include('ad', compact('ad'))
                @endforeach
            </div>
        </div>

@include('pagination')

@endsection
