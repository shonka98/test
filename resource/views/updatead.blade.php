@extends('masterUs')

@section('title', 'Главная')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Добавление объявления</div>

                <div class="card-body">
                    <form method="POST" action="#" aria-label="Login">
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Имя</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name"
                                       value="{{ $ad->name }}" required autofocus>
                            </div>

                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right" >Описание</label>

                            <div class="col-md-6">
                                <textarea id="name" type="text" class="form-control" name="description"
                                          value="" required autofocus> {{ $ad->description }}</textarea>
                            </div>
                        </div>
                        <!--
                                <div class="form-group row">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">Фото</label>

                                    <div class="col-md-6">
                                        <input id="name" type="file" class="form-control" name="file"
                                                  value="" required autofocus>
                                    </div>
                                </div>-->

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Категория</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="category"
                                       value="{{ $ad->category_id }}" required autofocus>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Цена</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="price"
                                       value="{{ $ad->price }}" required autofocus>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" name="submit" class="btn btn-primary">
                                    Добавить
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection