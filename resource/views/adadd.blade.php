@extends('masterUs')

@section('title', 'Главная')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Добавление объявления</div>
               <div class="card-header">Категория: <b> {{$nameCategory[0]}} / {{$nameCategory[1]}} / {{$nameCategory[2]}}</b></div>

                <div class="card-body">
                    <form   enctype="multipart/form-data" method="POST" action="#" aria-label="Login">
                        <div class="form-group row">

                            <label for="name" class="col-md-4 col-form-label text-md-right">Название объявления</label>

                           <div class="col-md-6">
                               <input id="name" type="text" class="form-control" name="name"
                                      value="" required autofocus>
                           </div>
                       </div>


                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Описание</label>

                            <div class="col-md-6">
                                <textarea id="name" type="text" class="form-control" name="description"
                                          value="" required autofocus> </textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Фото</label>

                            <div class="col-md-6">
                                <input type="hidden" name="MAX_FILE_SIZE" value="3000000000" />
                                    <input type="file" name="file_1" accept="image/*,image/jpeg" />
                                    <input type="file" name="file_2" accept="image/*,image/jpeg"/>
                                    <input type="file" name="file_3" accept="image/*,image/jpeg"/>
                                    <input type="file" name="file_4" accept="image/*,image/jpeg"/>
                                    <input type="file" name="file_5" accept="image/*,image/jpeg"/>
                            </div>

                                <ul>
                                    <!-- загрузки будут показаны здесь -->
                                </ul>


                            </div>
                        </div>




                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Цена</label>

                            <div class="col-md-6">
                                <input id="name" type="number" class="form-control" name="price"
                                       value="" required autofocus>
                            </div>
                        </div>
                 @csrf

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" name="submit" class="btn btn-primary">
                                    Добавить
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
