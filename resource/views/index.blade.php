@extends('master')

@section('title', 'Главная')

@section('content')

<div class="starter-template">
{{--    <table class="table">--}}
{{--        @foreach($categories as $category)--}}
{{--        <tr>--}}
{{--            <th>--}}
{{--                <h2> <a href="{{ \routes\Route::RouteName('category',$category['id'] )}}"> {{ $category['name'] }}</a> </h2>--}}
{{--            </th>--}}
{{--        </tr>--}}

{{--        @foreach( \app\models\Category::getLowCategory($category['id']) as $low)--}}
{{--        <tr>--}}
{{--            <td>--}}
{{--               <h4> <a href="{{  \routes\Route::RouteName('category',$category['id'] .'/'. $low['id'] )}}"> {{ $low['name'] }}</a> </h4>--}}
{{--            </td>--}}
{{--        </tr>--}}

{{--        @foreach(\app\models\Category::getLowerCategory($low['id']) as $lower)--}}
{{--        <tr>--}}
{{--            <td>--}}
{{--                <h6><a href="{{ \routes\Route::RouteName('category', $category['id'] .'/'. $low['id'] .'/'. $lower['id'] )}}">{{ $lower['name'] }}</a> </h6>--}}
{{--            </td>--}}
{{--        </tr>--}}
{{--            @endforeach--}}
{{--            @endforeach--}}
{{--        @endforeach--}}

{{--    </table>--}}

    <h1>Последние объявления</h1>
    <div class="row">
        <div class="row">
            @foreach($ads as $ad)
                @include('ad', compact('ad'))
            @endforeach
        </div>
        @include('pagination')
    </div>

@endsection