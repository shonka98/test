<?php


namespace components;
use eftec\bladeone\BladeOne;
use routes\Route;

class Auxiliary
{

    public static function routeActive($route)
    {
            $name = Route::getURI();
            $str = strripos($route, '*');
            $text = mb_strimwidth($route, 0, $str);
            $count = similar_text($text, $name);
            if($count == '') {
                echo  'class=active';
                }
                else {
                    echo ($name == '/'.$route) ? 'class=active' : '';
                }

    }

    public static function getLinks($text)
    {
        return  explode('@', $text);
    }

}