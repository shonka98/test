<?php


namespace components;


abstract class Validation
{
    public static function clearSTR($text)
    {
        return strip_tags(trim($text));
    }

    public static function checkName($name)
    {
        $name = self::clearSTR($name);
        $name = htmlspecialchars($name);
        if (strlen($name) >= 2) {
            if(strlen($name) <= 40){
                return $name;
            }
        }
        return false;
    }

    public static function checkEmail($email)
    {
        $email = self::clearSTR($email);
        $email = htmlspecialchars($email);
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return $email;
        }
        return false;
    }

    public static function checkPassword($password)
    {
        if (strlen($password) >= 4){
            if(preg_match('/^(\w{3,12})$/',$password))
                return $password;
        }
        return false;
    }

}