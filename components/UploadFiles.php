<?php


namespace components;


use app\models\Ads;
use app\models\User;

class UploadFiles
{

    public static function uploadFile($id_ads, $file_1, $file_2, $file_3, $file_4, $file_5)
    {

        $sellerId = User::checkLogged();
        $i = 100;
        $fileName = [];
        if($id_ads){
            if(!file_exists(ROOT.'/public/layouts/images/'.$sellerId)){
                mkdir(ROOT.'/public/layouts/images/'.$sellerId);
            }
            mkdir(ROOT.'/public/layouts/images/'.$sellerId.'/'.$id_ads);
        }
        $allowed = array('png', 'jpg');
        $files = array($file_1, $file_2, $file_3, $file_4, $file_5);
        foreach ($files as $file){
            if(!$file['size'] == 0){

                $i++;
                $extension = pathinfo($file['name'], PATHINFO_EXTENSION);

                if(!in_array(strtolower($extension), $allowed)){
                    $error =  'Пожалуйста выберите файл типа jpg,png,jpeg';
                }

                $fileN = strrev($file['name']);
                $pos = strripos($fileN, '.');
                $rest = substr($fileN, 0, $pos);
                $fileN = strrev($rest);
                $puth =  ROOT."/public/layouts/images/$sellerId/$id_ads/".$i.'.'.$fileN;

                if(move_uploaded_file($file['tmp_name'], $puth)){
                }
                $fileName[] = $puth;

            }
        }
        $fileName = implode('@', $fileName);
        Ads::insertImageLink($id_ads, $fileName);

    }

}