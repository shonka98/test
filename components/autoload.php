<?php
require_once ROOT.'/helpers/help.php';
function my_autoloader($class_name) {

    $array_paths = array(
        '/app/core/',
        '/app/controller/',
        '/app/models/',
        '/app/api/',
        '/routes/',
        '/helpers/',
        'components/',
    );

    foreach ($array_paths as $path) {
        $file = ROOT . '/' .str_replace('\\', '/', $class_name) . '.php';
        if(file_exists($file)){
            include_once $file;
        }
    }

}