<?php
namespace components;

use \PDO;
use \PDOException;

class Db
{
    public static function getConnection()
    {
        $paramsPath = ROOT. '/config/configDb.php';
        $params = include($paramsPath);

        $dsn = "mysql:host={$params['host']};dbname={$params['dbname']}";


        try{
            $db = new PDO ($dsn, $params['user'], $params['password'],  array(PDO::ATTR_DEFAULT_FETCH_MODE=>PDO::FETCH_OBJ,
                PDO::ATTR_ERRMODE=>TRUE));
        }catch(PDOExeception $e){

            echo 'Подключение не удалось: ' . $e->getMessage();

        }

        return $db;
    }

}