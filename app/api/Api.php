<?php

namespace app\api;

use app\models\Ads;
use app\api\GET;
class Api
{

    public function run()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        $formData = $this->getFormData($method);
        $url = (isset($_GET['q'])) ? $_GET['q'] : '';
        $url = rtrim($url, '/');
        $urls = explode('/', $url);
        $urlData = array_slice($urls, 1);
        $method = 'app\api\\'.$method;
        $new = new $method($this->array);
        $input = file_get_contents('php://input');
        $array = json_decode($input, true);
        $new->route($method, $urlData, $array);

    }

    public function getFormData($method)
    {
        // GET или POST: данные возвращаем как есть
        if ($method === 'GET') return $_GET;
        if ($method === 'POST') return $_POST;

        // PUT, PATCH или DELETE
        $data = array();
        $exploded = explode('&', file_get_contents('php://input'));

        foreach($exploded as $pair) {
            $item = explode('=', $pair);
            if (count($item) == 2) {
                $data[urldecode($item[0])] = urldecode($item[1]);
            }
        }

        return $data;
    }

    protected function getID($array)
    {
        foreach ($array as $item => $key) {
            if($item == 'id') return $key;
        }
    }

}