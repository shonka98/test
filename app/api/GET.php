<?php


namespace app\api;


use app\models\Ads;

class GET extends Api
{

    public function route($method, $url, $data)
    {
        $urlData = array_map('strtolower', $url);
        $method = $urlData[0];
        $this->$method($data);

    }

    public function ad($data)
    {
        $id = (int)$this->getID($data);
        $userData = Ads::getAdById($id)[0];
        http_response_code(200);
        echo json_encode(array(
            'id' => $userData->id,
            'name' => $userData->name,
            'price' => $userData->price,
            'seller_id' => $userData->seller_id
        ));

    }

}