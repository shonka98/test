<?php
namespace app\controller;

use app\core\Controller;
use app\models\Ads;
use app\models\Category;
use components\Pagination;

class MainController extends Controller
{

    private $argumenst;

    public function __construct($arguments)
    {
        $this->argumenst = $arguments;
    }

    public function action()
    {

        $page = str_replace('page-', '',$this->argumenst[0]);
        if($page == "") $page = 1;
        if(Ads::getTotalProductsByLimit() <= 90) $total = Ads::getTotalProductsByLimit();
        else $total = Ads::SHOW_LIMIT;
        $pagination = new Pagination($total, $page, Ads::SHOW_BY_DEFAULT, 'page-');
        @view('index', ['ads' => Ads::getLatestAds($page), 'pagination' => $pagination->get()]);
    }

    public function search()
    {
        $text = $_POST['text'];
        view('search', ['ads' => Ads::search($text)]);

    }


}

