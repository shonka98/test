<?php


namespace app\controller;


use app\models\Ads;
use app\models\Category;
use components\Pagination;

class CategoryController
{
    
    /**
     * @var mixed
     */
    private $page;
    /**
     * @var mixed
     */
    private $lowerCategoryId;
    /**
     * @var mixed
     */
    private $lowCategoryId;
    /**
     * @var mixed
     */
    private $categoryId;

    public function __construct($array)
        {
            $this->categoryId =  $array[0];
            $this->lowCategoryId = $array[1];
            $this->lowerCategoryId = $array[2];
            $this->page = $array[3];
        }

        public function action()
        {
            $page = str_replace('/page-', '',$this->page);
            if($page == "") $page = 1;
            $categories = array();
            $categories = Category::getCategoryList();
            $categoryProduct = array();
            $categoryProduct = Ads::getProductsListByCategory($this->categoryId, $this->lowCategoryId, $this->lowerCategoryId, $page);
            $total = Ads::getTotalProductsInCategory($this->categoryId, $this->lowCategoryId, $this->lowerCategoryId);
            $pagination = new Pagination($total, $page, Ads::SHOW_BY_DEFAULT, 'page-');
            $pagination1[] = array();
            $pagination = $pagination->get();



            @view('category', ['categories' => $categories, 'pagination' => $pagination, 'ads' => $categoryProduct]);
        }

        public function categories()
        {
            $categories = Category::getCategory();
            @view('categories', ['categories' => $categories]);
        }


}