<?php


namespace app\controller;


use app\models\Ads;
use app\models\Friend;
use app\models\User;
use components\Validation;
use components\hash;
use routes\Route;

class UserController
{

    public function register()
    {

        if(isset($_POST['submit'])) {
            $error = false;
            if(!$name = Validation::checkName($_POST['name'])){
                $error = 'Имя не может быть короче 2х символов';
            }

            if(!$email = Validation::checkEmail($_POST['email'])){
                $error = 'Неправильный email';
            }
            if(!$pass = Validation::checkPassword($_POST['password'])){
                $error = 'Неправильный Пароль';
            }
            if(!$error){
                $result = User::register($name, $email, Hash::EncryptPass($pass));
                if ($result) {
                    // Если данные правильные, запоминаем пользователя (сессия)
                    $this->setUser($result);
                    header("Location: /");
                }
            }
        }
        view('register', ['error' => $error]);
    }

    public function login()
    {
        if(isset($_POST['submit'])) {
            $error = false;
            if(!$email = $_POST['email']){
                $error = 'Неправильный email';
            }
            if(!$error){
                if($result = User::login($email)){
                    if (hash::DecryptPass($result->password ) === $_POST['password']) {
                        // Если данные правильные, запоминаем пользователя (сессия)
                        $this->setUser($result->id);
                    }
                }
            }
        }
        view('login', ['error' => $error]);
    }

    private function setUser($id)
    {
        $_SESSION['user'] = $id;
        setcookie("user", $id, strtotime("+30 days"));
        header("Location: /");
    }

    public function logout()
    {
        unset($_COOKIE['user']);
        setcookie('user', null, -1, '/');
        unset($_SESSION['user']);
        header("Location: /");
    }

    public function userPage($userId)
    {
        $user = User::getUserById($userId);
        $ads = Ads::getAdsBySellerId($userId);
        view('userpage', ['user' => $user, 'ads' => $ads]);
    }

    public function addSub($friendAddId)
    {

        $userId = User::checkLogged();
        if(Friend::checkFriendExists($userId, $friendAddId, 0)){
            Friend::addFriend($userId, $friendAddId, 1);
            header("Location: ".Route::RouteName('userpage', $friendAddId));
        } header("Location: /");

    }

    public function mypage()
    {
        $id = User::checkLogged();
        $adsNoPublic = Ads::getAdsBySellerId($id, 0);
        $adsPublic = Ads::getAdsBySellerId($id, 1);
        view('mypage', ['adsNoPublic' => $adsNoPublic, 'adsPublic' => $adsPublic]);
    }

    public function resetPass()
    {
        $error = false;
        $email = $_POST['email'];
        if(isset($_POST['resetPass'])) {
            if(!$error){
                if($result = User::login($email)){
                    mail("shonka98@Mail.ru", 'Да пошёл ты нахуй', 'Ну чтож Алмазбек, хотелось бы вам скачазть, чтоб вы пошли нахуй, всего хорошего');
                } else $error = 'Неправильный email';
            }
        }
        view('resetPass', ['error' => $error]);
    }

}