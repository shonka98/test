<?php


namespace app\controller;


use app\models\Ads;
use app\models\Category;
use app\models\User;
use components\UploadFiles;
use components\Validation;

class AdController
{


    private $arguments;

    public function __construct($arguments)
    {
        $this->arguments = $arguments;
    }

    public function add()
    {
        if(isset($_POST['submit'])) {
            $error = false;
            if(!$name = Validation::checkName($_POST['name'])){
                $error = 'Имя не может быть короче 2х символов';
            }
            $desc = $_POST['description'];


            if(empty($price = $_POST['price'])){
                $error = 'Введите сумму';
            }

            $sellerId = User::checkLogged();

            if(!$error){
                $id_ads = Ads::add($name, $desc, $price, $this->arguments, $sellerId);
                 UploadFiles::uploadFile($id_ads, $_FILES['file_1'], $_FILES['file_2'], $_FILES['file_3'], $_FILES['file_4'], $_FILES['file_5']);
                    if ($id_ads) {
                          header("Location: /");
                    }
            }
        }

        $nameCategory = Category::getCategoryName($this->arguments[0], $this->arguments[1], $this->arguments[2]);
        view('adadd', ['nameCategory' => $nameCategory[0]]);
    }


    public function selectCategory()
    {
        if(!$_COOKIE['user']) header("Location: login");
        $categories = Category::getCategory();
        @view('selectcategory', ['categories' => $categories]);
    }

    public function update()
    {

        $ad = Ads::getAdById($this->argumentsid);
        if(isset($_POST['submit'])) {
            $error = false;
            if(!$name = Validation::checkName($_POST['name'])){
                $error = 'Имя не может быть короче 2х символов';
            }
            $desc = $_POST['description'];


            if(empty($category = $_POST['category'])){
                $error = 'Имя не может быть короче 2х символов';
            }
            if(empty($price = $_POST['price'])){
                $error = 'Введите сумму';
            }

            $sellerId = User::checkLogged();

            if(!$error){
                $result = Ads::update($this->argumentsid, $name, $desc, $price, $category, $sellerId);
                if (!$result) {
                    header("Location: /");
                }
            }
        }

        view('updatead', ['ad' => $ad]);

    }

    public function delete()
    {
        Ads::delete($this->arguments);
        header("Location: /mypage");
    }

    public function ad()
    {
        if($ad = Ads::getAdById($this->arguments[0])){
            view('adpage', ['ad' => $ad[0], 'seller' => User::getUserById($ad[0]->seller_id)]);
        } else header("Location: /404");

    }




}