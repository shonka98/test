<?php


namespace app\controller;
use app\core\Controller;
use app\models\Admin;
use app\models\Ads;
use app\models\User;


class AdminController extends Controller
{

    private $arguments;

    public function __construct($arguments)
    {
        $this->arguments = $arguments;
    }

    public function login()
    {

        if(Admin::checkAdmin(User::checkLogged())){
            $ads = Ads::getAdsForAdmin();
            view('admin', ['ads' => $ads]);
        } else header("Location: /login");

    }

    public function publication()
    {
        if(Admin::checkAdmin(User::checkLogged())){
            Admin::publication($this->arguments[0]);
            header("Location: /admin");
        } else header("Location: /login");

    }

    public function reject()
    {
        if(Admin::checkAdmin(User::checkLogged())){
            Admin::reject($this->arguments[0]);
            $ads = Ads::getAdsForAdmin();
            view('admin', ['ads' => $ads]);
        } else header("Location: /login");

    }

}