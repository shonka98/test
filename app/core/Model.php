<?php


namespace app\core;



use Cassandra\Varint;
use components\Db;
use PDO;

class Model
{
    const STATUS = 1;

    public static function returned($data)
    {
        if($data) return $data;
        return false;
    }

    public static function query($sql, $param = null, $fetch = PDO::FETCH_ASSOC)
    {
        $db = Db::getConnection();
        $result = $db->prepare($sql);
        self::bindParam($result, $param);
        $result->execute();
        $data = $result->fetchAll($fetch);
        return self::returned($data);
    }

    public static function bindParam($result, $param)
    {
        foreach ($param as $item => &$key) {
            $result->bindParam($item, $key, PDO::PARAM_INT);
        }
    }

    public static function querySimple($sql, $param = null)
    {
        $db = Db::getConnection();
        $result = $db->prepare($sql);
        $result->execute($param);
        return $db->lastInsertId();
    }

    public static function queryWithout($sql, $param = null)
    {
        $db = Db::getConnection();
        $result = $db->prepare($sql);
        $result->execute($param);
    }


}