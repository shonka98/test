<?php


namespace app\models;

use app\core\Model;
use \PDO;
use components\Db;

class Category extends Model
{

    public static function getCategory()
    {

        $sql = 'SELECT * from category';
        return Model::query($sql);

    }


    public static function getCategoryList()
    {
        $sql = 'SELECT id, name FROM category ORDER BY id ASC';
        return Model::query($sql);
    }

    public static function getLowCategory($id)
    {
        $sql = 'SELECT * from `low_category` WHERE `key` = :id';
        $array = [':id' => $id];
        return Model::query($sql, $array);
    }

    public static function getLowerCategory($id)
    {
        $sql = 'SELECT * from lower_category WHERE `key` = :id';
        $array = [':id' => $id];
        return Model::query($sql, $array);
    }

    public static function getCategoryName($category_id, $low_category_id, $lower_category_id)
    {
        if($lower_category_id){
            $sql = 'SELECT category.name, low_category.name, lower_category.name 
                    FROM category, low_category, lower_category 
                    WHERE category.id = :category_id AND low_category.id = :low_category_id AND lower_category.id = :lower_category_id';
            $array = [':category_id' => $category_id,
                      ':low_category_id' => $low_category_id,
                      ':lower_category_id' => $lower_category_id];
        }else {
            if($low_category_id){
                $sql = 'SELECT category.name, low_category.name FROM category, low_category 
                        WHERE category.id = :category_id AND low_category.id = :low_category_id';
                $array = [':category_id' => $category_id, ':low_category_id' => $low_category_id];
            } else {$sql = "SELECT category.name FROM category WHERE category.id = :category_id";
                $array = [':category_id' => $category_id];
                }
        }
        return Model::query($sql, $array , PDO::FETCH_BOTH);


    }

}