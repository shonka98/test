<?php
namespace app\models;


use app\core\Model;
use components\Db;
use PDO;

class User extends Model
{

    public static function register($name, $email, $pass)
    {

        // Соединение с БД
        $db = Db::getConnection();

        // Текст запроса к БД
        $sql = 'INSERT INTO users (name, email, password)'
             . 'VALUES (:name, :email, :password)';

        // Получение и возврат результатов. Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':name', $name, PDO::PARAM_STR);
        $result->bindParam(':email', $email, PDO::PARAM_STR);
        $result->bindParam(':password', $pass, PDO::PARAM_STR);
        $result->execute();
        $id = $db->lastInsertId();


        if($id == 0){
            return false;
        }
        return $id;
    }

    public static function login($email)
    {
        $db = Db::getConnection();
        $sql = 'SELECT * from users WHERE email = :email';
        $result = $db->prepare($sql);
        $result->bindParam(':email', $email, PDO::PARAM_STR);
        $result->execute();
        $user = $result->fetch();
        if($user){
            return $user;
        }
        return false;

    }

    public static function getUserById($id)
    {
        // Соединение с БД
        $db = Db::getConnection();

        // Текст запроса к БД
        $sql = 'SELECT * from users WHERE id = :id';

        // Получение и возврат результатов. Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->execute();

        $user = $result->fetch();
        if($user){
            return $user;
        }
        return false;
    }


    public static function checkLogged()
    {
        // Если сессия есть, вернем идентификатор пользователя
        if (isset($_COOKIE['user'])) {
            return $_COOKIE['user'];
        }

        header("Location: /register");
    }

}