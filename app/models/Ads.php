<?php
namespace app\models;


use app\core\Model;
use components\Date;
use components\Db;
use PDO;

class Ads extends Model
{

    const SHOW_BY_DEFAULT = 6;
    const SHOW_LIMIT = 60;


    public static function getLatestAds($page)
    {
        $page = intval($page);
        $offset = ($page - 1) * self::SHOW_BY_DEFAULT;
        $sql = 'SELECT id, name, price, img, date FROM announcement WHERE status = 1 ORDER BY id DESC LIMIT ' . self::SHOW_BY_DEFAULT . ' OFFSET :OFFSET';
        $array = [":OFFSET" => $offset];
        return Model::query($sql, $array, $fetch = null);
    }


    public static function getAdsBySellerId($id, $status)
    {
        $sql = 'SELECT * from announcement WHERE seller_id = :seller_id AND status = :status';
        $array = [':seller_id' => $id,
                  ':status' => $status];
        return Model::query($sql, $array, $fetch = null);
    }

    public static function add($name, $desc, $price, $category, $sellerId)
    {
        if($category[1]){
            $low = ' low_category_id,';
            $param = ' :low_category_id,';
        }
        if($category[2]){
            $lower = ' lower_category_id,';
            $paramer = ' :lower_category_id,';
        }

        $sql = 'INSERT INTO announcement (name, description, `date`, category_id,' . $low . $lower . 'price, seller_id)'
            . 'VALUES (:name, :description, :date, :category_id,' . $param . $paramer . ':price,  :seller_id)';

        $array = [':name' => $name,
                  ':description' => $desc,
                  ':price' => $price,
                  ':seller_id'=> $sellerId,
                  ':date' => Date::getTodayDate()];


        if($category[1]){
            $array += [':category_id' => $category[0]];
            $array += [':low_category_id' => $category[1]];
        } else{
            $array += [':category_id' => $category];
        }
        if($category[2])  $array += [':lower_category_id' => $category[2]];
        return Model::querySimple($sql, $array);
    }

    public static function insertImageLink($id, $imgLink)
    {
        $sql = 'UPDATE  announcement SET img = :img WHERE id = :id';
        $array = [':id' => $id,
                  ':img' => $imgLink];
        Model::queryWithout($sql, $array);
    }

    public static function getAdById($id)
    {

        $STATUS = self::STATUS;
        $sql = 'SELECT * from announcement WHERE id = :id AND status = :status';
        $array = [
            ':id' => $id,
            ':status' => $STATUS
            ];
        return Model::query($sql, $array, $fetch = null);
    }


    public static function update($id, $name, $desc, $price, $category, $sellerId)
    {

        // Соединение с БД
        $db = Db::getConnection();
        // Текст запроса к БД
        $sql = 'UPDATE  announcement SET name = :name, description = :description, category_id = :category_id, price = :price, seller_id = :seller_id WHERE id = :id';

        // Получение и возврат результатов. Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':name', $name, PDO::PARAM_STR);
        $result->bindParam(':description', $desc, PDO::PARAM_STR);
        $result->bindParam(':category_id', $category, PDO::PARAM_INT);
        $result->bindParam(':price', $price, PDO::PARAM_INT);
        $result->bindParam(':seller_id', $sellerId, PDO::PARAM_INT);
        $result->execute();

    }

    public static function delete($id)
    {

        // Соединение с БД
        $db = Db::getConnection();
        // Текст запроса к БД
        $sql = 'DELETE FROM announcement WHERE announcement . id = :id';

        // Получение и возврат результатов. Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->execute();

    }

    public static function getAdsForAdmin()
    {
        // Соединение с БД
        $db = Db::getConnection();

        // Текст запроса к БД
        $sql = 'SELECT * from announcement WHERE status = 0';

        // Получение и возврат результатов. Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->execute();

        $ads = $result->fetchAll(PDO::FETCH_ASSOC);

        if($ads){
            return $ads;
        }
        return false;
    }

    public static function search($text)
    {
        // Соединение с БД
        $db = Db::getConnection();
        $text = '%'.$text.'%';
        // Текст запроса к БД
        $sql = 'SELECT * FROM `announcement` WHERE `description` LIKE :text ';

        // Получение и возврат результатов. Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':text', $text, PDO::PARAM_STR);
        $result->execute();

        $ads = $result->fetchAll();

        if($ads){
            return $ads;
        }
        return false;
    }


    public static function getByCategory($category_id, $low_category_id, $lower_category_id)
    {

        $db = Db::getConnection();
        $limit = self::SHOW_BY_DEFAULT;
        if($low_category_id){
            $and1 = "AND `low_category_id` = :low_category_id";
        }else $and1 = NULL;
        if($lower_category_id){
            $and2 = " AND `lower_category_id` = :lower_category_id";
        }else $and2 = NULL;
        $sql = 'SELECT * FROM `announcement` WHERE `category_id` = :category_id ' . $and1 . $and2 . ' AND status = 1 ORDER BY id DESC LIMIT :limit';

        $result = $db->prepare($sql);
        $result->bindParam(':category_id', $category_id, PDO::PARAM_INT);
        $result->bindParam(':limit', $limit, PDO::PARAM_INT);
        if($low_category_id)
        $result->bindParam(':low_category_id', $low_category_id, PDO::PARAM_INT);
        if($lower_category_id)
        $result->bindParam(':lower_category_id', $lower_category_id, PDO::PARAM_INT);

        $result->execute();

        $ads = $result->fetchAll();

        if($ads){
            return $ads;
        }
        return false;
    }


    public static function getProductsListByCategory($category_id, $low_category_id, $lower_category_id, $page)
    {

        if($category_id){
            $page = intval($page);
            $offset = ($page - 1) * self::SHOW_BY_DEFAULT;


            $db = Db::getConnection();
            if($low_category_id){
                $and1 = "AND `low_category_id` = :low_category_id";
            }else $and1 = NULL;
            if($lower_category_id){
                $and2 = " AND `lower_category_id` = :lower_category_id";
            }else $and2 = NULL;
            $sql = 'SELECT * FROM `announcement` WHERE `category_id` = :category_id '
                . $and1 . $and2 .
                ' AND status = 1  ORDER BY id DESC LIMIT ' . self::SHOW_BY_DEFAULT . ' OFFSET :OFFSET';
            $result = $db->prepare($sql);
            $result->bindParam(':category_id', $category_id, PDO::PARAM_INT);
            if($low_category_id)
                $result->bindParam(':low_category_id', $low_category_id, PDO::PARAM_INT);
            if($lower_category_id)
                $result->bindParam(':lower_category_id', $lower_category_id, PDO::PARAM_INT);
                $result->bindParam(':OFFSET', $offset, PDO::PARAM_INT);

            $result->execute();

            $ads = $result->fetchAll();

            if($ads){
                return $ads;
            }
            return false;

        }


    }


    public static function getTotalProductsInCategory($category_id, $low_category_id, $lower_category_id)
    {
        $db = Db::getConnection();


        if($low_category_id){
            $and1 = "AND `low_category_id` = :low_category_id";
        }else $and1 = NULL;
        if($lower_category_id){
            $and2 = " AND `lower_category_id` = :lower_category_id";
        }else $and2 = NULL;
        $sql = 'SELECT count(id) as count  FROM `announcement` WHERE `category_id` = :category_id '
            . $and1 . $and2 . ' AND status = 1';
        $result = $db->prepare($sql);
        $result->bindParam(':category_id', $category_id, PDO::PARAM_INT);
        if($low_category_id)
            $result->bindParam(':low_category_id', $low_category_id, PDO::PARAM_INT);
        if($lower_category_id)
            $result->bindParam(':lower_category_id', $lower_category_id, PDO::PARAM_INT);

        $result->execute();

        $ads = $result->fetchAll();

        return $ads[0]->count;
    }



    public static function getTotalProductsByLimit()
    {
        $db = Db::getConnection();
        $limit = SELF::SHOW_LIMIT;

        $sql = 'SELECT count(id) as count  FROM `announcement` WHERE status = 1 LIMIT 10';
        $result = $db->prepare($sql);
        $result->bindParam(':LIMIT', $limit, PDO::PARAM_INT);

        $result->execute();

        $ads = $result->fetchAll();
        return $ads[0]->count;
    }


}