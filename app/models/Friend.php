<?php


namespace app\models;
use components\Db;
use PDO;

class Friend
{

    public static function checkFriendExists($userId, $friendId, $status)
    {

        // Соединение с БД
        $db = Db::getConnection();

        // Текст запроса к БД
        $sql = 'SELECT COUNT(*) FROM friend WHERE user_id = :user_id AND friend_id = :friend_id AND status = :status';

        // Получение результатов. Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':user_id', $userId);
        $result->bindParam(':friend_id', $friendId);
        $result->bindParam(':status', $status);
        $result->execute();
        if ($result->fetchColumn()){
            return false;
        }
        return true;

    }

    public static function addFriend($userId, $friendId, $status)
    {

        $db = Db::getConnection();

        $sql = 'INSERT INTO friend (user_id, friend_id, status) '
            .             ' VALUES (:user_id, :friend_id, :status)';

        $result = $db->prepare($sql);
        $result->bindParam(':user_id', $userId);
        $result->bindParam(':friend_id', $friendId);
        $result->bindParam(':status', $status);
        $result->execute();

    }



}