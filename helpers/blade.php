<?php
use eftec\bladeone\BladeOne;
include  "vendor/eftec/bladeone/lib/BladeOne.php";
include "app/core/Controller.php";

function view($view, $array)
{
    $views =  'resource/views';
    $compiledFolder = __DIR__ . '/compiled';
    $blade=new BladeOne($views,$compiledFolder,BladeOne::MODE_DEBUG);
    echo $blade->run("$view", $array); // /views/hello.blade.php must exist
}